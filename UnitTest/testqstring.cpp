#include "testqstring.h"

#include <QDebug>

TestQString::TestQString()
{
    qDebug() << "Constructor";
}

void TestQString::toUpper1()
{
    QString str = "Hello";
    QVERIFY(str.toUpper() == "HELLO");
    // QVERIFY(false);
}

void TestQString::toUpper2()
{
    QString str = "Hello";
    QCOMPARE(str.toUpper(), QString("HELLO"));
}

void TestQString::toUpper3(QString str)
{
    QCOMPARE(str.toUpper(), QString("HELLO"));
}

void TestQString::toUpper4_data()
{
    QTest::addColumn<QString>("string");
    QTest::addColumn<QString>("result");

    QTest::newRow("all lower") << "hello" << "HELLO";
    QTest::newRow("mixed")     << "Hello" << "HELLO";
    QTest::newRow("all upper") << "HELLO" << "HELLO";

    qDebug() << "CHECK 3";
}

void TestQString::toUpper4()
{
    QFETCH(QString, string);
    QFETCH(QString, result);
    QCOMPARE(string.toUpper(), result);
}

void TestQString::testGui()
{
    QLineEdit lineEdit;

    QTest::keyClicks(&lineEdit, "hello world");

    QCOMPARE(lineEdit.text(), QString("hello world"));
}

void TestQString::testGui2_data()
{
    QTest::addColumn<QTestEventList>("events");
    QTest::addColumn<QString>("expected");

    QTestEventList list1;
    list1.addKeyClick('a');
    QTest::newRow("char") << list1 << "a";

    QTestEventList list2;
    list2.addKeyClick('a');
    list2.addKeyClick(Qt::Key_Backspace);
    QTest::newRow("there and back again") << list2 << "x";
}

void TestQString::testGui2()
{
    QFETCH(QTestEventList, events);
    QFETCH(QString, expected);

    QLineEdit lineEdit;

    events.simulate(&lineEdit);

    QCOMPARE(lineEdit.text(), expected);
}

QTEST_MAIN(TestQString)
// QTEST_APPLESS_MAIN(TestQString)
