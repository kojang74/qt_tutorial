#ifndef TESTQSTRING_H
#define TESTQSTRING_H

#include <QObject>
#include <QLineEdit>
#include <QtTest/QtTest>

class TestQString : public QObject
{
    Q_OBJECT

public:
    TestQString();

private slots:
    void toUpper1();
    void toUpper2();
    void toUpper3(QString str);
    void toUpper4_data();
    void toUpper4();

    void testGui();
    void testGui2_data();
    void testGui2();
};

#endif // TESTQSTRING_H
