#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;

    QString title = "Hello, "
                    "World!";
    w.setWindowTitle(title);

    w.show();
    return a.exec();
}
