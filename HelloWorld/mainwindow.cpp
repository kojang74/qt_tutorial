#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    QString title = "Hello!!, @@@@@@ "
                    "World!!!";

    QObject::connect(this, SIGNAL(sigOk()), this, SLOT(onOk()));

    emit sigOk();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onOk()
{
    qDebug() << "Ok...";
}

void MainWindow::on_buttonHoho_clicked()
{
    this->hohoho();
}

void MainWindow::hohoho()
{
    qDebug() << "Hohoho...";
}
