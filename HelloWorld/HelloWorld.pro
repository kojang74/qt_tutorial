QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11 my_config_test

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

message($${QT_VERSION})
message($${QT_MAJOR_VERSION})
message($${QT_MINOR_VERSION})
message($${QT_PATCH_VERSION})

message($${QT})
message($${CONFIG})
message($${SOURCES})
message($${HEADERS})
message($${FORMS})
message($$first(CONFIG))

my_config_test {
    message(My CONFIG test.)
    message("'My CONFIG test.'")
    message('"My CONFIG test."')
}


MESSAGE = This is a tent.
message($$replace(MESSAGE, tent, test))
MESSAGE2 = $$replace(MESSAGE, tent, test2)
message($${MESSAGE2})

UNAME = $$system(dir /?)
message($$CONFIG)
# message($${UNAME})
