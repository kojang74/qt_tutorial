#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    void hohoho();

signals:
    void sigOk();
    void sigOkOk();
    void sigOkOkOk();
    void sigOkOkOkOk();
    void sigOkOkOkOkOk();

public slots:
    void onOk();

private slots:
    void on_buttonHoho_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
