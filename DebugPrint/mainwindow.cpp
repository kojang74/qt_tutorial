#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    qDebug("This is a debug message 1.");
    qDebug() << "This is a debug message 2.";

    qDebug() << "This is a debug message : " << QString::number(3);

    qInfo() << "This is a informational message.";
    qWarning() << "This is a warning message.";
//    qCritical() << "This is a critical error message.";
//    qFatal("This is a fatal error message.");
}

MainWindow::~MainWindow()
{
    delete ui;
}

