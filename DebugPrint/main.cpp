#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    qSetMessagePattern("[%{type}] saaring (%{time hh:mm:ss.zzz}:%{function}:%{line}) - %{message}");
//    qSetMessagePattern("[%{time yyyyMMdd h:mm:ss.zzz t} %{if-debug}D%{endif}%{if-info}I%{endif}%{if-warning}W%{endif}%{if-critical}C%{endif}%{if-fatal}F%{endif}] %{file}:%{line} - %{message}");

    qputenv("QT_FATAL_WARNINGS", QByteArray("1"));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
