#include "myobject.h"

#include <QDebug>

MyObject::MyObject(QString myName, QObject *parent)
    : myName(myName)
    , QObject(parent)
{
    qDebug() << myName << " : MyObject constructor...";

    connect(&timer, &QTimer::timeout, this, &MyObject::onTimeout);
    // timer.start(1000);
}

MyObject::~MyObject()
{
    qDebug() << myName << " : MyObject destructor...";
}

void MyObject::printMyName()
{
    qDebug() << myName << " : MyObject printMyName...";
}

void MyObject::onTimeout()
{
    qDebug() << myName << " : MyObject onTimeout...";
}
