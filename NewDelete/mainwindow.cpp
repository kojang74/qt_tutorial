#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , myObject("Stack using object")
{
    qDebug() << "Mainwindow constructor...";

    pMyObject1 = new MyObject("Heap using object(delete style, no parent)");
    // delete pMyObject1;

    pMyObject2 = new MyObject("Heap using object(delete style, set parent)", this);
    // delete pMyObject2;

    pMyObject3 = new MyObject("Heap using object(deleteLater style, no parent)");
    // pMyObject3->deleteLater();

    pMyObject4 = new MyObject("Heap using object(deleteLater style, set parent)", this);
    // pMyObject4->deleteLater();

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    // delete pMyObject;

    delete ui;

    qDebug() << "MainWindow destructor...";
}


void MainWindow::on_buttonOk_clicked()
{
    qDebug() << "MainWindow ok...";
}
