#ifndef MYOBJECT_H
#define MYOBJECT_H

#include <QObject>
#include <QTimer>

class MyObject : public QObject
{
    Q_OBJECT
public:
    explicit MyObject(QString myName, QObject *parent = nullptr);
    ~MyObject();

    void printMyName();

signals:

private slots:
    void onTimeout();

private:
    QTimer timer;
    QString myName;
};

#endif // MYOBJECT_H
