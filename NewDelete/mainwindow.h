#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "myobject.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_buttonOk_clicked();

private:
    Ui::MainWindow *ui;

    MyObject myObject;
    MyObject *pMyObject1;
    MyObject *pMyObject2;
    MyObject *pMyObject3;
    MyObject *pMyObject4;
};
#endif // MAINWINDOW_H
